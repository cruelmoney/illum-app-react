# ILLUM KEY - React app

## API endpoints

Har ikke fastsat nogle URLs, så det står frit til at beslutte. 

Eksempler på data objekterne findes i `src/data`. 

Har mocked GET endpoints til følgende entiteter:

- user
  - Returneres ved login og indeholder en `auth_token`, som kan bruges ved alle efterfølgende kald. 
- keys
- brandliste
- purchase history

Og følgende POST endpoints:

- indløse key 
  - Returnerer OK, eller error object.
- opdatere profil
  - Returnerer updated user, eller error object.
- oprette profil
  - Returnerer user, eller error object.  


Der bliver tjekket for et error object i JSON responsen, og hvis den findes, bliver error.message renderet ud i appen. 



## React crash course
Alle ændringer laves i `./src`. Skriv `yarn start` i terminalen, for at starte en lokal development server, hvor ændringerne kan ses.

Kør `yarn build` for at "compile" projeketet til mappen `./build`. Html filen i denne mappe skal loades ind i concrete5 for at køre appen. 