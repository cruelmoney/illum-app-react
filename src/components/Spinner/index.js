import React, { Component } from 'react';

class Spinner extends Component {
  render() {
    const {active, absolute, noBg} = this.props;

    return (
      <div className={`spinner ${active ? "loading" : ""} ${absolute ? "absolute" : ""}`}>
        <div className={`spinner-bg ${noBg ? "no-bg" : ""}`}>
            <div className={`spinner-circle`}></div>
        </div>
      </div>
    );
  }
}

export default Spinner;