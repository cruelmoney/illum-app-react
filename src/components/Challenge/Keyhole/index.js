import React, { PureComponent } from 'react';
import Hole from './Keyhole';
import keyProvider from '../../../providers/illumkey';
import Spinner from '../../Spinner';

class Keyhole extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            finished: props.finished,
            win: false
        }
    }

    componentDidMount(){
        if(!this.props.disabled){
            this.challengeResult();
        }
    }

    componentWillUnmount(){
     
    }


    challengeResult = async () => {
        const {onFinish, checkIsWinner} = this.props; 
        this.setState({
            finished: true,
            loading: true
        });
        try {
            const data = await checkIsWinner();
            this.setState({
                loading: false,
                rewardImage: data.prize.thumb,
                win: true
            });
            onFinish && onFinish(data);
        } catch (error) {
            this.setState({
                loading: false,
                win: false
            });
            onFinish &&  onFinish(error);
        }
    }

    render() {
        const { image, defaultImage } = this.props;
        const {finished, loading, rewardImage, win} = this.state;
        return (
            <div className="keyhole">
                <div 
                    className={`draggable_wp ${finished ? "finished" : ""}`}>
                    <Hole
                        id="drag-hole"
                        image={image || defaultImage}
                        onTouchStart={this.onDragStart}
                        onMouseDown={this.onDragStart}
                    />
                </div>
                <div 
                    className={`no-reward ${win ? "hide" : ""}`}>
                    <Spinner absolute active={loading} noBg />
                    <Hole
                        id="no-reward-hole"
                        image={defaultImage}
                    />
                </div>
                {
                    !win ? null : ( 
                        <div 
                            className={`reward`}>
                            <Hole
                                id="reward-hole"
                                image={rewardImage}
                            />
                        </div>
                    )
                }
            
                
            </div>
        );
    }
}

Keyhole.propTypes = {

};

export default keyProvider(Keyhole);