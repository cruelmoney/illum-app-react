import React from "react";

const Keyhole = ({image, fill, ...props}) => {
  return <svg
    viewBox="0 0 147 239"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    {...props}
  >
    <defs>
      <path
        d="M32.104 134.242C12.722 121.008 0 98.74 0 73.5 0 32.907 32.907 0 73.5 0S147 32.907 147 73.5c0 25.24-12.722 47.508-32.104 60.742L147 238.35H0l32.104-104.108z"
        id={props.id + "-b"}
      />
     
      {
        !image ?
        <pattern
        id={props.id + "-pattern"}
        width={28.32}
        height={28.32}
        x={-28.32}
        y={-28.32}
        patternUnits="userSpaceOnUse"
      />
        :
        <pattern
        id={props.id + "-pattern"}
        height={"100%"}
        width={"100%"}
        viewBox="0 0 1 1"
        preserveAspectRatio="xMidYMid slice" 
        patternContentUnits="objectBoundingBox"
      >
              <image
                id={`${props.id}-a`}
                width={1}
                height={1}
                xlinkHref={image}
                preserveAspectRatio="xMidYMid slice" 

              />
      </pattern>
      }
    </defs>
    <g fillRule="nonzero" fill="none">
      {!!fill && <use fill={fill} xlinkHref={`#${props.id}-b`} />}
      <use fill={`url(#${props.id}-pattern)`} xlinkHref={`#${props.id}-b`} />
    </g>
  </svg>
};

export default (Keyhole);