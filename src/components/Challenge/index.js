import React, { PureComponent } from "react";
import Keyhole from "./Keyhole";
import { Flipper, Flipped } from "react-flip-toolkit";
import IllumError from "../../utils/IllumError";
import "./index.css";
import BackButton from "../Header/BackButton";
import NoScroll from "../NoScroll";
import keyImage from "./Keyhole/eye";

const Content = ({
	onClick,
	disabled,
	onFinish,
	challengeInformation,
	image
}) => (
	<Flipped inverseFlipId={"square"}>
		<div className="reverse-animation">
			<Flipped flipId="square-back" opacity>
				<div className="challenge-close">
					<BackButton showBackButton={true} onClick={onClick} />
				</div>
			</Flipped>
			<Flipped flipId="square-header" translate>
				<h1>ILLUM KEY CHALLENGE</h1>
			</Flipped>
			<Flipped flipId="square-hole" translate scale>
				<div>
					<Keyhole
						defaultImage={keyImage}
						image={image}
						onFinish={onFinish}
						disabled={disabled}
					/>
				</div>
			</Flipped>
			<Flipped flipId="square-information" translate opacity>
				<div className="challenge-information">
					<p>{challengeInformation}</p>
				</div>
			</Flipped>
			<Flipped flipId="square-content" translate opacity>
				<div className="challenge-button">
					<button className="appbtn" onClick={onClick}>
						Se om du har vundet
					</button>
				</div>
			</Flipped>
		</div>
	</Flipped>
);

const IntitalChallenge = ({ onExit, onComplete, finishedGame, ...props }) => (
	<Flipped flipId="square" onComplete={onExit}>
		<div className="intial-challenge challenge">
			<Content {...props} finishedGame={finishedGame} disabled />
		</div>
	</Flipped>
);

const FullChallenge = ({ onExit, onComplete, ...props }) => (
	<Flipped flipId="square" onComplete={onComplete}>
		<div className="full-challenge challenge">
			<Content {...props} />
			<NoScroll />
		</div>
	</Flipped>
);

class Challenge extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			fullScreen: false,
			gameActive: false,
			challengeInformation:
				"Kig gennem nøglehullet og se om du har vundet dagens voucher."
		};
	}

	componentWillReceiveProps(props) {
		if (!props.canPlay && !this.state.finishedGame) {
			this.setState({
				challengeInformation: "Du har allerede kigget i nøglehullet i dag.",
				finishedGame: true
			});
		}
	}

	toggleFullScreen = () => {
		this.setState(prevState => ({
			fullScreen: !prevState.fullScreen
		}));
	};

	onComplete = elem => {
		this.setState({
			gameActive: true
		});
	};
	onExit = elem => {
		this.setState({
			gameActive: false
		});
	};

	onFinish = async data => {
		const { onComplete } = this.props;
		if (data instanceof IllumError) {
			this.setState({
				challengeInformation: data.message,
				finishedGame: true,
				image: keyImage
			});
		} else {
			this.setState({
				challengeInformation: data.prize.description,
				finishedGame: true,
				image: data.prize.thumb
			});
		}
		await onComplete();
	};

	render() {
		const {
			gameActive,
			challengeInformation,
			image,
			finishedGame
		} = this.state;

		return (
			<div
				className={`keyhole-challenge ${gameActive ? "active" : ""} ${
					finishedGame ? "finished" : ""
				}`}
			>
				<Flipper flipKey={this.state.fullScreen}>
					{this.state.fullScreen ? (
						<FullChallenge
							finishedGame={finishedGame}
							challengeInformation={challengeInformation}
							onFinish={this.onFinish}
							onExit={this.onExit}
							onComplete={this.onComplete}
							onClick={this.toggleFullScreen}
						/>
					) : (
						<IntitalChallenge
							image={image}
							finishedGame={finishedGame}
							onExit={this.onExit}
							challengeInformation={challengeInformation}
							onComplete={this.onComplete}
							onClick={this.toggleFullScreen}
						/>
					)}
				</Flipper>
			</div>
		);
	}
}

export default Challenge;
