import { PureComponent } from 'react';

class NoScroll extends PureComponent {

  componentDidMount = () => {
    setTimeout(() => {
      const container = document.body;
      container && container.classList.add("no-scroll");
    }, 500);
    
  }
  componentWillUnmount = () => {
    const container = document.body;
    container && container.classList.remove("no-scroll");
  }

  render = _ => null
}
export default NoScroll;