import React, { Component } from 'react';

class Swiper extends Component {

  constructor(props){
    super(props);
    this.maxValue     = 100; // the higher the smoother when dragging
    this.speed        = 12; // thanks to @pixelass for this
    this.rafID        = null;
    this.clipper      = React.createRef();
    this.inputRange   = React.createRef();
    this.clipperInner = React.createRef();
    this.state        = {
                          completed:false,
                          completeText:props.completeText
                        }
  }

  componentDidMount(){
    this.inputRange.current.value = 0;
  }

  onChange = (e) => {
    this.clipText(e.target.value);
  }

  unlockEndHandler = (e) => {
    const currValue = e.target.value;

    // determine if we have reached success or not
    if (currValue >= this.maxValue) {
      this.successHandler();
    } else {
      this.rafID = window.requestAnimationFrame(()=>this.resetAnimateHandler(currValue-this.speed));
    }
  }

  // handle range animation
  resetAnimateHandler = (nextVal) => {
    this.inputRange.current.value = nextVal;
    if(nextVal > -1){
      this.clipText(nextVal);
      window.requestAnimationFrame(()=>this.resetAnimateHandler(nextVal-this.speed));
    }else{
      window.cancelAnimationFrame(this.rafID);    
    }
  }

  // handle successful unlock
  successHandler = () => {
    const { completed } = this.state;
    const { complete, onComplete, loadingText } = this.props;
    const isComplete = complete || completed;
    if(!isComplete){
      this.setState({
        completed: true,
        completeText: loadingText
      });
      onComplete((t)=>{
        this.setState({
          completeText: t
        });
      });
    }
  };

  // handle clipping text animation
  clipText = (currValue) => {
    this.clipper.current.style.transform = 'translateX(' + currValue + '%)';
    this.clipperInner.current.style.transform = 'translateX(-' + currValue + '%)';
  }

  render() {
    const { completed, completeText } = this.state;
    const { complete, swipeText } = this.props;

    const isComplete = complete || completed;
    return (
      <div className={`voucher-swipe ${isComplete ? "complete" : ""}`}>
          {
            isComplete ?
            <div className="completed">
              {completeText}
            </div>
            : null
        }
        <div className="info">
          <div className="clipper" ref={this.clipper} >
              <div className="clipper-inner" ref={this.clipperInner} >
                {swipeText}
              </div>
          </div>
        </div>
        <input 
          disabled={isComplete}
          onChange={this.onChange}
          onMouseUp={this.unlockEndHandler}
          onTouchEnd={this.unlockEndHandler}
          ref={this.inputRange}
          type="range" 
          min="0"
          max="100"
          className="pullee" 
        />
      </div>
    );
  }
}

export default Swiper;