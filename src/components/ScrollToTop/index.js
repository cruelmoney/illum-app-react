import { Component } from 'react';
import { withRouter } from 'react-router-dom';

class ScrollToTop extends Component {
  componentDidMount() {
      const container = document.body;
      container && container.scrollTo(0, 0);
  }

  render() {
    return null
  }
}

export default withRouter(ScrollToTop)