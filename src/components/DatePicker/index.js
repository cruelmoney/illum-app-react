import React, { Component } from 'react';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import './index.css';
import { YearPicker, MonthPicker, DayPicker } from 'react-dropdown-date';

class IllumDatePicker extends Component {
  constructor(props){
    super(props);
    const {selected} = props;
    if(selected){
      this.state={
        day: selected.format('D'),
        month: String(Number(selected.format('M'))-1) ,
        year: selected.format('YYYY'),
      }
    }
  }
  state={}

  onChange = () => {
    const {onChange} = this.props;
    const {day, month, year} = this.state;
    if(day && month && year){
      const date = new moment(`${day}-${Number(month)+1}-${year}`, 'D-M-YYYY');
      onChange && onChange(date);
    }
  } 

  onMonthChange = (e) => {
    this.setState(state => ({
      month:e,
      // day: state.resetOnChange ? null : state.day,
      // year: state.resetOnChange ? null : state.year,
      // resetOnChange: false
    }), this.onChange);
    
  }
  onDayChange = (e) => {

    this.setState(state => ({
      day:e,
      // month: state.resetOnChange ? null : state.month,
      // year: state.resetOnChange ? null : state.year,
      // resetOnChange: false
    }), this.onChange);
  }
  onYearChange = (e) => {
    this.setState(state => ({
      year:e,
      // day: state.resetOnChange ? null : state.day,
      // month: state.resetOnChange ? null : state.month,
      // resetOnChange: false
    }), this.onChange);
  }
  
  onDateChange = (e) => {
    const {onChange} = this.props;
    const date = new moment(e);
    onChange && onChange(date);
  }


  render() {
    const {month, day, year} = this.state;

    return (
      <div id="dropdown-date">
        <div>
          <YearPicker
            defaultValue={'Fødsel År'}
            // default is 1900
            //start={2010}
            // default is current year
            //end={2020}
            // default is ASCENDING
            reverse
            // mandatory
            value={this.state.year}
            // mandatory
            onChange={this.onYearChange}
            id={'year'}
            name={'year'}
            classes={`classes ${year ? "" : "unselected"}`}
            optionClasses={'option classes'}
          />
        </div>
        <div>
          <MonthPicker
            defaultValue={'Måned'}
            // default is full name
            short
            // default is Titlecase
            // mandatory if end={} is given in YearPicker
            endYearGiven
            // mandatory
            year={this.state.year}
            // mandatory
            value={this.state.month}
            // mandatory
            onChange={this.onMonthChange}
            id={'month'}
            name={'month'}
            classes={`classes ${month ? "" : "unselected"}`}
            optionClasses={'option classes'}
          />
        </div>
        <div>
          <DayPicker
            defaultValue={'Dag'}
            // mandatory
            year={this.state.year}
            // mandatory
            month={this.state.month}
            // mandatory if end={} is given in YearPicker
            endYearGiven
            // mandatory
            value={this.state.day}
            // mandatory
            onChange={this.onDayChange}
            id={'day'}
            name={'day'}
            classes={`classes ${day ? "" : "unselected"}`}
            optionClasses={'option classes'}
          />  
        </div> 
         
      </div>
        // <DropdownDate
        //   startDate={                         // optional, if not provided 1900-01-01 is startDate
        //     null                      // 'yyyy-mm-dd' format only
        //   }
        //   endDate={                           // optional, if not provided current date is endDate
        //     null                              // 'yyyy-mm-dd' format only
        //   }
        //   onMonthChange={this.onMonthChange}  // optional
        //   onDayChange={this.onDayChange}      // optional
        //   onYearChange={this.onYearChange}    // optional
        //   onDateChange={this.onDateChange}    // optional
        //   names={                             // optional
        //     {
        //       year: 'year',
        //       month: 'month',
        //       day: 'day'
        //     }
        //   }
        //   classes={                           // optional
        //     {
        //       dateContainer: 'classes',
        //       yearContainer: 'classes',
        //       monthContainer: 'classes',
        //       dayContainer: 'classes',
        //       year: `classes ${year ? "" : "unselected"}`,
        //       month:  `classes ${month ? "" : "unselected"}`,
        //       day:  `classes ${day ? "" : "unselected"}`,
        //       yearOptions: 'classes',
        //       monthOptions: 'classes',
        //       dayOptions: 'classes'
        //     }
        //   }
        //   defaultValues={                     // optional
        //     {
        //       year: year || 'Fødsel År',
        //       month: month || 'Måned',
        //       day: day || 'Dag'
        //     }
        //   }
       
        //   options={                           // optional
        //     {
        //       yearReverse: true,              // false by default
        //       monthShort: true,               // false by default
        //       monthCaps: false                 // false by default
        //     }
        //   }
        // />
      
      // <DatePicker
      //     selected={moment().subtract(30,'years')}
      //     dateFormat="DD/MM/YYYY"
      //     {...this.props}
      //     readOnly
      //     showYearDropdown
      //     scrollableYearDropdown
      //     maxDate={moment()}
      //     withPortal
      //     yearDropdownItemNumber={50}
      // />     
    );
  }
}

export default IllumDatePicker;