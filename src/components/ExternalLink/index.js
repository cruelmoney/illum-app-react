import React from 'react';

const ExternalLink = ({href, ...props}) => {

  const openLink = (e) => {
    e.preventDefault();
    window.open(href, '_system');
  }

  return (
    <a 
      onClick={openLink}
      href={href}
      {...props} 
    >
      {props.children}
    </a>
  )
};

export default ExternalLink;
