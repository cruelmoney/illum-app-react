import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import {RouteContext} from '../../App';
import BackButton from './BackButton';

const easeOutQuad = (x, t, b, c, d) => {
  return c * t / d + b;
}
class Header extends Component {
  constructor(props){
    super(props);
    this.header = React.createRef();
    this.scrolled = false;
    this.state={
      scrolled:false
    }
  }

  componentDidMount = () => {
    const container = document;
    container.addEventListener('scroll', this.handleScroll);
    container.addEventListener('touchmove', this.handleScroll);

  }
  componentWillUnmount() {
    const container = document;
    container.removeEventListener('scroll', this.handleScroll);
    container.removeEventListener('touchmove', this.handleScroll);
  }

  handleScroll = (e) => {
    const amount = document.documentElement.scrollTop || document.body.scrollTop;
    if(amount > 10 && !this.scrolled){
      this.scrolled = true;
      this.setState({
        scrolled: true
      })
    }else if(amount <= 10 && this.scrolled){
      this.scrolled = false;
      this.setState({
        scrolled: false
      })
    }
  }

  cancelAnimations = () => {
    this.inFrame && window.cancelAnimationFrame(this.inFrame);
    this.outFrame && window.cancelAnimationFrame(this.outFrame);
  }

  animateIn = (t = 0) => {
    if( t<= 10 ){
        const opacity = easeOutQuad(null, t, 0, 1, 10);
        this.header.current.style.opacity = opacity;
        this.inFrame = window.requestAnimationFrame( _ => this.animateIn(t+1));
    }
  }

  animateOut = (t = 0) => {
    if( t<= 10 ){
        const opacity = easeOutQuad(null, t, 1, -1, 10);
        this.header.current.style.opacity = opacity;
        this.outFrame = window.requestAnimationFrame( _ => this.animateOut(t+1));
    }
  }
  
  render() {
    const { scrolled } = this.state;
    const { history } = this.props;

    return (
      <RouteContext.Consumer>
        {({routeName, hideHeader, showBackButton, scrollAnimation}) => (
           <header 
           
           className={`section-header ${hideHeader ? 'hidden' : ""} ${scrollAnimation  ? 'show-bg' : ""}`}>
              <BackButton onClick={history.goBack} showBackButton={showBackButton} />
              <h1>{routeName}</h1>
              {scrolled ?   <div className="bg" /> : null}
            
          </header>
        )}
      </RouteContext.Consumer>
    )
  }
};

export default withRouter(Header);