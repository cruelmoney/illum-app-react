import React from 'react';

export default ({showBackButton, onClick}) => (
  <a 
    className={`back-btn ${showBackButton ? null : 'hidden'}`} 
    onClick={onClick} >
    <svg viewBox="0 0 20.6 38">
        <path fill="none" className="arrow" stroke="#b29761" strokeLinecap="round" strokeLinejoin="round" strokeWidth="4" d="M18.5 2L2 18.9 18.6 36"/>
    </svg>
  </a>
)
