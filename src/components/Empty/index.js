import React, { Component } from 'react';
import './index.css';

export default class Empty extends Component {
  render() {
    return (
      <div className="empty">
        <h2>
          {this.props.message}
        </h2>
      </div>
    )
  }
};
