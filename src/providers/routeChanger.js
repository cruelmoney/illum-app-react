import React, {Component} from 'react';

const defaultContext = {
  routeName: '',
  hideHeader: false,
  showBackButton: false,
  scrollAnimation: false,
  disableScroll: false,
  profile: {},
  loading:false,
  vouchers:[],
  changeRoute: () => {},
  setProfile: () => {},
  setError: () => {}
}

const RouteContext = React.createContext(defaultContext);

const routeChanger = (Wrappee) => {
  return class extends Component{
    render(){
      return (
        <RouteContext.Consumer>
          {
            ({changeRoute, showBackButton, profile, setError, vouchers}) => (
              <Wrappee 
                {...this.props} 
                vouchers={vouchers}
                profile={profile}
                setError={setError}
                changeRoute={changeRoute}
                showBackButton={showBackButton}
              />
            )
          }
        </RouteContext.Consumer>
      )
    }
  }
}

export default routeChanger;
export {
  RouteContext,
  defaultContext
}