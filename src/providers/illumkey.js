import React from "react";
import IllumError from "../utils/IllumError";

/**
 * @param  {} WrappedComponent
 * This component exposes methods for fetching data. It shouldn't be
 * responsible for changing state in any way.
 * Will exopse the following props for the wrapped component:
 * userLogin    ({email, password}) -> Promise {{...profile}, status, user_id, user_token, timestamp}
 * checkLogin   _ -> Promise {{...profile}, status, user_id, user_token, timestamp}
 * getVouchers  _ -> Promise {[...vouchers]}
 * getHistory   _ -> Promise {_type, MemberNo, [...Transaction] }
 */
export default WrappedComponent => {
	const domain = "https://key.illum.dk/api";

	const mockRequest = async ({ throwError } = {}) => {
		// eslint-disable-line no-unused-vars
		return new Promise((resolve, reject) => {
			setTimeout(_ => {
				if (throwError) throw new IllumError();
				resolve();
			}, 1000);
		});
	};

	const handleError = e => {
		console.log(e);
		if (e instanceof IllumError) {
			throw e;
		} else {
			throw new IllumError(e.toString());
		}
	};

	const handleResponse = response => {
		var contentType = response.headers.get("content-type");
		if (contentType && contentType.includes("application/json")) {
			return response
				.json()
				.then(data => {
					if (!response.ok) {
						if (!!data.errorMessage) {
							throw new IllumError(data.errorMessage);
						} else {
							throw new IllumError();
						}
					}
					return data;
				})
				.catch(handleError);
		} else {
			throw new TypeError("Response not JSON!");
		}
	};

	const params = data => {
		// first remove empty vals
		Object.keys(data).forEach(
			k => !data[k] && data[k] !== undefined && delete data[k]
		);
		return Object.keys(data)
			.map(key => `${key}=${encodeURIComponent(data[key])}`)
			.join("&");
	};

	const parseProfile = data => {
		if (!data.profile || !data.user_id || !data.user_token)
			throw new IllumError("Token mangler");
		const profile = {
			...data.profile,
			user_id: data.user_id,
			user_token: data.user_token
		};
		return profile;
	};

	const parseVoucher = voucher => {
		return {
			...voucher,
			cashed: voucher.iscashed
		};
	};

	const parseBrandlist = data => {
		return data.map(d => ({
			...d,
			rawtext: !!d.rawtext && d.rawtext.trim()
		}));
	};

	const parseTerms = data => {
		return data[0].content;
	};

	const onLogin = profile => {
		localStorage.setItem("profile", JSON.stringify(profile));
		if (window.in_app && !!window.pushwoosh) {
			window.pushwoosh.setUserId(profile.user_id);
		}
		return profile;
	};

	const userLogin = ({ email, password }) => {
		return fetch(domain + `/User/Login?email=${email}&password=${password}`)
			.then(handleResponse)
			.then(parseProfile)
			.then(onLogin)
			.catch(handleError);
	};

	const checkLogin = async (user_id, user_token, returnProfile) => {
		return await fetch(
			domain + `/User?user_id=${user_id}&user_token=${user_token}`
		)
			.then(handleResponse)
			.then(data => {
				if (window.in_app && !!window.pushwoosh) {
					window.pushwoosh.setUserId(user_id);
				}
				if (!returnProfile) return data;
				data.user_token = user_token;
				return parseProfile(data);
			})
			.catch(e => false);
	};

	const checkLoginFromStored = async ({ returnProfile } = {}) => {
		// get from localhost and check against endpoint
		const storedProfile = localStorage.getItem("profile");
		if (!storedProfile) return false;
		const profile = JSON.parse(storedProfile);
		return await checkLogin(profile.user_id, profile.user_token, returnProfile);
	};

	const getVouchers = async _ => {
		const data = await checkLoginFromStored();
		if (!data) throw new IllumError("Ikke logget ind");
		if (!data.vouchers) throw new IllumError("No vouchers");
		return data.vouchers.map(parseVoucher);
	};

	const getHistory = async _ => {
		const data = await checkLoginFromStored();
		if (!data) throw new IllumError("Ikke logget ind");
		if (!data.history) throw new IllumError("No history");
		return {
			...data.history,
			Transaction: data.history.Transaction || []
		};
	};

	const userLogout = async _ => {
		return localStorage.removeItem("profile");
	};

	const cashVoucher = async ({ data }) => {
		const query = params(data);
		const profile = await checkLoginFromStored({ returnProfile: true });
		if (!data) throw new IllumError("Ikke logget ind");
		return fetch(
			domain +
				`/User/CashVoucher?user_id=${profile.user_id}&user_token=${
					profile.user_token
				}&${query}`
		)
			.then(handleResponse)
			.then(d => {
				console.log(d);
			})
			.catch(handleError);
	};

	const getBrandList = async _ => {
		return fetch(domain + `/Content/GetBrandlist`)
			.then(handleResponse)
			.then(data => data.brandlists)
			.then(parseBrandlist)
			.catch(handleError);
	};

	const getTermsAndConditions = async _ => {
		return fetch(domain + `/Content/GetTermsAndConditions`)
			.then(handleResponse)
			.then(data => data.content)
			.then(parseTerms)
			.catch(handleError);
	};

	const forgotPassword = async ({ data }) => {
		const query = params(data);
		return fetch(domain + `/User/ForgotPass?${query}`)
			.then(handleResponse)
			.catch(handleError);
	};

	const createUser = async ({ data }) => {
		const query = params(data);
		return await fetch(domain + `/User/Create?${query}`)
			.then(handleResponse)
			.then(async d => {
				const profile = await checkLogin(d.user_id, d.user_token, true);
				onLogin(profile);
				return profile;
			})
			.catch(handleError);
	};

	const updateUser = async ({ data }) => {
		let profile = await checkLoginFromStored({ returnProfile: true });
		if (!profile) throw new IllumError("Ikke logget ind");
		const query = params(data);
		return await fetch(
			domain +
				`/User/Update?user_id=${profile.user_id}&user_token=${
					profile.user_token
				}&${query}`
		)
			.then(handleResponse)
			.then(async d => {
				const profile = await checkLogin(d.user_id, d.user_token, true);
				onLogin(profile);
				return profile;
			})
			.catch(handleError);
	};

	const fbLogin = () =>
		new Promise((resolve, reject) => {
			if (!window.in_app) {
				return reject(new IllumError("Not supported outside app."));
			}
			window.app.fbLogin(function(response) {
				if (response.status === "connected") {
					const access_token = response.authResponse.accessToken;
					fetch(domain + `/User/FBLogin?access_token=${access_token}`)
						.then(handleResponse)
						.then(parseProfile)
						.then(onLogin)
						.then(resolve)
						.catch(e => reject(handleError(e)));
				} else {
					reject(new IllumError());
				}
			});
		});

	const checkIsWinner = async () => {
		let profile = await checkLoginFromStored({ returnProfile: true });
		if (!profile) throw new IllumError("Ikke logget ind");
		return await fetch(
			domain +
				`/User/CheckIsWinner?user_id=${profile.user_id}&user_token=${
					profile.user_token
				}`
		)
			.then(handleResponse)
			.catch(handleError);
	};

	const getContent = async () => {
		return await fetch(domain + `/Content`)
			.then(handleResponse)
			.then(d => {
				delete d.timestamp;
				return d;
			})
			.catch(handleError);
	};

	const hocComponent = ({ ...props }) => (
		<WrappedComponent
			userLogin={userLogin}
			checkLogin={checkLoginFromStored}
			userLogout={userLogout}
			getHistory={getHistory}
			getVouchers={getVouchers}
			forgotPassword={forgotPassword}
			createUser={createUser}
			updateUser={updateUser}
			cashVoucher={cashVoucher}
			getBrandList={getBrandList}
			fbLogin={fbLogin}
			checkIsWinner={checkIsWinner}
			onLogin={onLogin}
			getTermsAndConditions={getTermsAndConditions}
			getContent={getContent}
			{...props}
		/>
	);

	return hocComponent;
};
