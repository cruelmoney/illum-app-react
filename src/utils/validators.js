import React from 'react';
import validator from 'validator';

const required = (value) => {
  if (!value.toString().trim().length) {
    // We can return string or jsx as the 'error' prop for the validated Component
    return <span className="error">Required</span>
  }
};
 
const email = (value) => {
  if (!validator.isEmail(value)) {
    return <span className="error">Not a valid email</span>
  }
};
 
const lt = (value, props) => {
  // get the maxLength from component's props
  const l = value.toString().trim().length;
  if (l > props.maxLength || l < props.minLength) {
    // Return jsx
    return <span className="error">The value exceeded {props.maxLength} symbols.</span>
  }
};

const phone = (value) => {
  const length = value.toString().trim().length;
  if (length < 8) {
    // Return jsx
    return <span className="error">Not a valid phone number.</span>
  }
}
 
const password = (value, props, components) => {
  if (value !== components['password'][0].value) { // components['password'][0].value !== components['confirm'][0].value
    // 'confirm' - name of input
    // components['confirm'] - array of same-name components because of checkboxes and radios
    return <span className="error">Passwords are not equal.</span>
  }
};

export {
  password,
  lt,
  email,
  required,
  phone
}