export default class IllumError extends Error {
  constructor(message, ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    message = !!message ? message : "Noget gik galt";
    super(message, ...params);
    if(Error.captureStackTrace){
      Error.captureStackTrace(this, IllumError);
    }
    Object.setPrototypeOf(this, IllumError.prototype);
  }
}