
const setPushwooshUserId = userid => {
  window.pushwoosh.setUserId(userid);
}

const isInPhongap = _ => !!window.in_app;
  
export {
  setPushwooshUserId,
  isInPhongap
}