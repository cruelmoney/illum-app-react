import React, { Component } from "react";
import MainNav from "./components/MainNav";
import Header from "./components/Header";
import Home from "./routes/Home";
import Profile from "./routes/Profile";
import EditProfile from "./routes/EditProfile";
import Card from "./routes/Card";
import Keys from "./routes/Keys";
import Login from "./routes/Login";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { RouteContext, defaultContext } from "./providers/routeChanger";
import Spinner from "./components/Spinner";
import IllumProvider from "./providers/illumkey";
import { createBrowserHistory } from "history";
import FastClick from "fastclick";

FastClick.attach(document.body);

const history = createBrowserHistory({
	/* pass a configuration object here if needed */
});
window.pushPath = history.push;

class App extends Component {
	constructor(props) {
		super(props);
		// Optimistically use stored profile
		// Might be removed at validating login after mount
		const storedProfile = localStorage.getItem("profile");
		const profile = !!storedProfile ? JSON.parse(storedProfile) : {};
		this.state = {
			...defaultContext,
			profile: profile,
			changeRoute: this.changeRoute
		};
	}

	componentDidMount = async () => {
		// Check if user token is still valid
		let profile = await this.props.checkLogin({
			returnProfile: true
		});
		profile = profile || {};
		this.props.onLogin(profile);
		this.setState({
			profile: profile
		});
	};

	changeRoute = options => {
		this.setState(state => {
			return {
				...defaultContext,
				...state,
				...options
			};
		});
	};

	render() {
		const { profile, disableScroll } = this.state;
		const { user_token } = profile;
		return (
			<Router history={history}>
				<RouteContext.Provider value={this.state}>
					{!user_token ? (
						<Route path="/" component={Login} />
					) : (
						<main className={`app ${disableScroll ? "no-scroll" : ""}`}>
							<Header />
							<div className="content">
								<Switch>
									<Redirect from="/register" to="/home" />
									<Route exact path="/profile" component={Profile} />
									<Route path="/profile/edit" component={EditProfile} />
									<Route path="/card" component={Card} />
									<Route path="/keys/:id?" component={Keys} />
									<Route path="/home" component={Home} />
									<Route component={Home} />
								</Switch>
							</div>
							<MainNav />
						</main>
					)}
					<Spinner active={this.state.loading} />
				</RouteContext.Provider>
			</Router>
		);
	}
}
export { RouteContext, history };
export default IllumProvider(App);
