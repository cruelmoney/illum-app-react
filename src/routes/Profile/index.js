import React, { Component } from "react";
import routeChanger from "../../providers/routeChanger";
import { NavLink } from "react-router-dom";
import IllumProvider from "../../providers/illumkey";
import "./index.css";

const routeOptions = {
	routeName: "PROFIL",
	hideHeader: false,
	scrollAnimation: false
};

class Profile extends Component {
	state = {
		flip: false
	};
	componentDidMount = () => {
		const { changeRoute } = this.props;
		changeRoute(routeOptions);
	};

	logOut = () => {
		const { changeRoute, userLogout } = this.props;
		userLogout();
		changeRoute({ profile: {}, vouchers: [] });
		window.pushPath("/");
	};

	toggleFlip = () => {
		this.setState(state => ({
			flip: !state.flip
		}));
	};

	render() {
		const { flip } = this.state;
		const { profile } = this.props;
		const {
			name,
			memberno,
			member_level,
			email,
			gender,
			phone,
			birthday,
			points,
			pointsvalueformatted,
			pointsvalueraw
		} = profile;

		return (
			<div className="key-profile" id="profile">
				<div className="points">
					<div className={`points-square ${flip ? "flip" : ""}`}>
						<div className={`points-flipper`} onClick={this.toggleFlip}>
							<div className="front">
								<h3>POINTSALDO</h3>
								<h1 className="white">{points}</h1>
								<span className="flip-btn">Vis saldo i kr</span>
							</div>
							<div className="back">
								<h3>SALDO</h3>
								<h1 className="white">{pointsvalueformatted}</h1>
								<span className="flip-btn-reverse">Vis saldo i point</span>
							</div>
						</div>
					</div>
				</div>
				<div className="user-details-wrapper">
					<div className="user-details-header">Mine oplysninger</div>
					<div className="user-details">
						<div className="user-name">
							{name}{" "}
							<span className="log-out">
								<a onClick={this.logOut}>Log ud</a>
							</span>
						</div>
						<div className="user-details-grid">
							<div className="user-id">
								<span>Medlemsnummer</span>
								<span>{memberno}</span>
							</div>
							<div className="user-level">
								<span>Medlemsniveau</span>
								<span>{member_level}</span>
							</div>
							<div className="user-mail">
								<span>E-mail</span>
								<span>{email}</span>
							</div>
							<div className="user-phone">
								<span>Telefon</span>
								<span>{phone}</span>
							</div>
							<div className="user-birthday">
								<span>Fødselsdag</span>
								<span>{birthday}</span>
							</div>
							<div className="user-sex">
								<span>Køn</span>
								<span>{gender}</span>
							</div>
						</div>
						<NavLink
							to={"/profile/edit"}
							id="update-details"
							className="user-update-btn"
						>
							Edit
						</NavLink>
					</div>
				</div>
			</div>
		);
	}
}

export default routeChanger(IllumProvider(Profile));
