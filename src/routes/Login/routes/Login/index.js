import React, { PureComponent } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from  'react-validation/build/button';
import * as validator from '../../../../utils/validators';
import IllumProvider from '../../../../providers/illumkey';
import IllumError from '../../../../utils/IllumError';
import {Link} from 'react-router-dom';
import changeRoute from '../../../../providers/routeChanger';
import {history} from '../../../../App';

class Login extends PureComponent {
  constructor(props){
    super(props);
    
    this.state={
      password: null,
      email: null
    }
    this.form       = React.createRef();
    this.formButton = React.createRef();
  }

  handleChange = (e) => {
    const {name, value} = e.target
    this.setState({
        [name] : value
    });
  }


  handleLogin = async (e) => {
    e.preventDefault();
    const self = this;
    this.form.current.validateAll();
    const values = this.form.current.getValues();
    const {changeRoute, userLogin} = this.props;
    changeRoute({loading:true});
    this.removeError();
    try {
      const profile = await userLogin(values);
      changeRoute({
        loading: false,
        profile: profile
      });
      history.push('/home');
    } catch (error) {
      changeRoute({loading: false});
      const message =  (error instanceof IllumError) ? error.message : "Noget gik galt";
      self.showError(message);
    }
  }

  fbLogin = async (e) => {
    e.preventDefault();
    const self = this;
    const {changeRoute, fbLogin} = this.props;
    changeRoute({loading:true});
    this.removeError();
    try {
      const profile = await fbLogin();
      changeRoute({
        loading: false,
        profile: profile
      });
      history.push('/home');
    } catch (error) {
      changeRoute({loading: false});
      const message =  (error instanceof IllumError) ? error.message : "Noget gik galt";
      self.showError(message);
    }
  }

  showError = message => {
    this.form.current.showError(this.formButton.current, <span className="error">{message}</span>);
  }

  removeError = () => {
    this.form.current.hideError(this.formButton.current);
  }

  render() {
    const {match} = this.props;
    const {email, password} = this.state;
    return (
      <React.Fragment>
      <div  className="authentication-type authentication-type-illumkey">
        <div data-scene="login">
          <div className="cont">
          <Form ref={this.form} onSubmit={this.handleLogin}>
              <Input
              type="email" 
              name="email" value={email}  
              placeholder="Indtast e-mail" 
              validations={[validator.required, validator.email]}
              />
              <Input 
                ref={this.formButton}
              type="password" 
              name="password" 
              value={password}  
              validations={[validator.required]}
              placeholder="Indtast password" />
              <Button 
                type="submit">
                LOG IND
              </Button>
            <div className="utility-links">
              <span>
                <Link to={`${match.url}register`} >
                Ny bruger?
                </Link>
              </span>
              <span>
                <Link to={`${match.url}forgot`} >
                Glemt adgangskode?
                </Link>
              </span>
            </div>
            </Form>
          </div>
        </div>
      </div>
      <div data-handle="facebook" className="authentication-type authentication-type-facebook">
        <div className="center-block text-center marginbottom">
          <a onClick={this.fbLogin} className="btn btn-illum button facebook-btn"> 
            Log ind med facebook
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
      </div>
    </React.Fragment>
    );
  }
}

Login.propTypes = {

};

export default changeRoute(IllumProvider(Login));