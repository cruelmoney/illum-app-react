import React, { PureComponent } from 'react';
import {Link} from 'react-router-dom';
import IllumProvider from '../../../../providers/illumkey';
import IllumError from '../../../../utils/IllumError';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from  'react-validation/build/button';
import * as validator from '../../../../utils/validators';
import changeRoute from '../../../../providers/routeChanger';

class ForgotPassword extends PureComponent {
  constructor(props){
    super(props);
    
    this.state={
      password: null,
      email: null,
      message: null
    }
    this.form       = React.createRef();
    this.formButton = React.createRef();
  }

  handleChange = (e) => {
    const {name, value} = e.target
    this.setState({
        [name] : value
    });
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const self = this;
    this.form.current.validateAll();
    const values = this.form.current.getValues();
    const {forgotPassword, changeRoute} = this.props;
    changeRoute({loading:true});

    this.removeError();
    try {
      await forgotPassword({data:values});
      changeRoute({loading:false});

      this.setState({
        message: 'Email er afsendt'
      });
    } catch (error) {
      changeRoute({loading:false});
      const message =  (error instanceof IllumError) ? error.message : "Noget gik galt";
      self.showError(message);
    }
  }

  showError = message => {
    this.form.current.showError(this.formButton.current, <span className="error">{message}</span>);
  }

  removeError = _ => {
    this.form.current.hideError(this.formButton.current);
  }
  
  render() {
    const {email, message} = this.state;

    return (
      <React.Fragment>
          <div data-handle="illumkey" className="authentication-type authentication-type-illumkey">
            <div data-scene="password">
              <div className="cont">
                <h2>Nulstil adgangskode</h2>
                  <Form 
                  ref={this.form}
                  onSubmit={this.handleSubmit}
                  >
                    <label htmlFor="reset">Indtast din e-mail herunder, så sender vi dig et link til, hvordan du nulstiller din adgangskode.</label>
                    <Input 
                    ref={this.formButton}
                    type="email" 
                    name="email" 
                    value={email}  
                    placeholder="Indtast e-mail" 
                    validations={[validator.required, validator.email]}
                    />
                    <Button type="submit">Nulstil adgangskode</Button>
                    <h2 className="api-message">{message}</h2>
                  </Form>

                <div className="utility-links">
                  <span>
                    <Link to="/" >
                      Gå til login
                    </Link>
                  </span>
                </div>
              </div>
            </div>
          </div>
      </React.Fragment>
    );
  }
}

export default changeRoute(IllumProvider(ForgotPassword));