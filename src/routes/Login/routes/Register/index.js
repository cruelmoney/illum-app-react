import React, { PureComponent } from "react";
import ExternalLink from "../../../../components/ExternalLink";
import { Link } from "react-router-dom";
import DatePicker from "../../../../components/DatePicker";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";
import Select from "react-validation/build/select";
import * as validator from "../../../../utils/validators";
import IllumProvider from "../../../../providers/illumkey";
import routeChanger from "../../../../providers/routeChanger";
import IllumError from "../../../../utils/IllumError";

class Register extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			email: "",
			gender: "",
			phone: "",
			birthday: "",
			accept_terms: "",
			accept_newsletter: "",
			message: ""
		};
		this.form = React.createRef();
		this.formButton = React.createRef();
	}

	handleChange = e => {
		const { name } = e.target;
		const val =
			e.target.type === "checkbox" ? e.target.checked : e.target.value;
		this.setState({
			[name]: val
		});
	};

	submit = async e => {
		e.preventDefault();
		const self = this;
		const { changeRoute, createUser } = this.props;
		this.form.current.validateAll();
		const values = this.form.current.getValues();

		const { birthday, accept_terms, accept_newsletter } = this.state;

		if (!accept_terms) {
			return self.showError("Please accept terms");
		}

		if (!!birthday) {
			values.birthday = birthday.format("DD/MM/YYYY");
		}

		const data = {
			...values,
			general_permission: accept_newsletter,
			accept_terms
		};

		changeRoute({ loading: true });
		try {
			const profile = await createUser({ data });
			changeRoute({
				loading: false,
				profile: profile
			});
		} catch (error) {
			changeRoute({ loading: false });
			const message =
				error instanceof IllumError ? error.message : "Noget gik galt";
			self.showError(message);
		}
	};

	showError = message => {
		this.setState({
			message
		});
	};

	removeError = () => {
		this.setState({
			message: ""
		});
	};

	render() {
		const { gender, birthday, message } = this.state;

		return (
			<div data-scene="register">
				<div className="cont">
					<Form ref={this.form} onSubmit={this.submit} className="form-stacked">
						<fieldset>
							<div className="form-group">
								<label htmlFor="name" className="control-label">
									Navn
								</label>
								<Input
									type="text"
									id="name"
									name="name"
									placeholder="Indtast dit fulde navn"
									className="form-control ccm-input-text"
									validations={[validator.required]}
								/>
							</div>
						</fieldset>

						<fieldset>
							<div className="form-group">
								<label htmlFor="email" className="control-label">
									E-mail adresse
								</label>
								<Input
									type="text"
									id="email"
									name="email"
									placeholder="Indtast din email adresse"
									className="form-control ccm-input-text"
									validations={[validator.required, validator.email]}
								/>
							</div>
							<div
								className="ccm-profile-attribute"
								style={{ marginBottom: "13px" }}
							>
								<div className="form-group">
									<label htmlFor="phone" className="control-label">
										Telefon
									</label>
									<div className="clearfix">
										<div className="intl-tel-input allow-dropdown">
											<div className="flag-container">
												<div
													className="selected-flag"
													tabIndex="0"
													title="Denmark (Danmark): +45"
												>
													<div className="iti-flag dk" />
													<div className="iti-arrow" />
												</div>
											</div>
											<Input
												placeholder="Indtast dit telefonnummer"
												type="tel"
												id="phone"
												name="phone"
												autoComplete="off"
												validations={[validator.required]}
											/>
										</div>
									</div>
								</div>
							</div>

							<div className="form-group">
								<label htmlFor="password" className="control-label">
									adgangskode
								</label>
								<Input
									placeholder="Skriv adgangskode"
									validations={[validator.required]}
									type="password"
									id="password"
									name="password"
									autoComplete="off"
									className="form-control ccm-input-password"
								/>
							</div>

							<div className="form-group">
								<label htmlFor="confirmPassword" className="control-label">
									Bekræft adgangskode
								</label>

								<Input
									type="password"
									id="confirmPassword"
									name="confirmPassword"
									autoComplete="off"
									className="form-control ccm-input-password"
									validations={[validator.required, validator.password]}
									placeholder="Bekræft adgangskode"
								/>
							</div>

							<div className="row">
								<div className="col-xs-6">
									<div className="form-group">
										<label htmlFor="birthday" className="control-label">
											Fødselsdag
										</label>
										<DatePicker
											placeholderText="Fødselsdag"
											name="birthday"
											selected={birthday}
											onChange={m =>
												this.handleChange({
													target: {
														name: "birthday",
														value: m
													}
												})
											}
										/>
									</div>
								</div>
								<div className="col-xs-6">
									<div className="form-group">
										<label htmlFor="gender" className="control-label">
											Køn
										</label>
										<Select
											id="gender"
											name="gender"
											onChange={this.handleChange}
											className={
												"form-control " + (!gender ? "unselected" : "selected")
											}
										>
											<option value="">KØN</option>
											<option value="Kvinde">Kvinde</option>
											<option value="Mand">Mand</option>
										</Select>
									</div>
								</div>
							</div>

							<div className="control control-checkbox">
								<input
									id="accept_terms"
									name="accept_terms"
									value="1"
									type="checkbox"
									onChange={this.handleChange}
								/>
								<label htmlFor="accept_terms">
									Ja tak. Meld mig ind i kundeklubben ILLUM KEY.
									<br />
									<ExternalLink href="https://key.illum.dk/application/files/3615/2715/3635/ILLUM_KEY_Terms_and_Conditions_15.5.18.pdf">
										Se vilkår og betingelser
									</ExternalLink>
								</label>
							</div>

							<div className="control control-checkbox">
								<input
									id="accept_newsletter"
									name="accept_newsletter"
									value="1"
									onChange={this.handleChange}
									type="checkbox"
								/>
								<label htmlFor="accept_newsletter">
									<strong>Ja selvfølgelig</strong> vil jeg gerne modtage:
									<br />
									<ul className="key-bullets">
										<li>Gratis ILLUM vouchers.</li>
										<li>Tilbud og nyheder.</li>
										<li>Invitationer til events og konkurrencer.</li>
										<li>
											Gratis services f.eks. skrædderservice og gratis levering
											til døren af dine indkøb m.m.
										</li>
									</ul>
									<p>
										Du får det hele via. e-mail, sms, app-notifikaitoner,
										sociale medier, eller kommunikations-apps.
									</p>
									<p>
										Dit samtykke kan til enhver tid tilbagekaldes og du kan
										redigere, slette, få udleveret dine personlige data på "Mit
										ILLUM KEY" og afmelde dig kommunikation direkte via links i
										bunden af e-mails og SMS.
									</p>
									<p>
										ILLUM behandler dine personoplysninger for at kunne sende
										dig ovennævnte kommunikation. Læs mere i vores
										<ExternalLink href="https://key.illum.dk/application/files/1415/2714/8233/ILLUM_KEY_Persondatapolitik_15.5.18_-_rettet_NR.pdf">
											{" "}
											persondatapolitik.
										</ExternalLink>
									</p>
								</label>
							</div>
						</fieldset>

						<div>
							<div className="form-actions clearfix">
								<Button type="submit">TILMELD</Button>
								<span className="error">{message}</span>
							</div>
							<div className="utility-links">
								<span>
									Allerede medlem? <Link to="/">Login</Link>
								</span>
							</div>
						</div>
					</Form>
				</div>
			</div>
		);
	}
}

export default routeChanger(IllumProvider(Register));
