import React from 'react';
import logo from '../../assets/img/ik-logo.png';
import {Switch, Route} from 'react-router-dom';
import Login from './routes/Login';
import ForgotPassword from './routes/ForgotPassword.js';
import Register from './routes/Register';

export default props => {
  const { match } = props;
  return (
    <div className="login-screen">
      <header>
        <img className="logo" src={logo} alt="logo" />
      </header>
      <div className="scenes">
        <Switch>
          <Route path={`${match.url}forgot`} component={ForgotPassword} />
          <Route path={`${match.url}register`} component={Register} />
          <Route path={`${match.url}`} component={Login} />
        </Switch>
      </div>
    </div>
  );
}
