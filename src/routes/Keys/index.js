import React, { PureComponent } from "react";
import routeChanger from "../../providers/routeChanger";
import VoucherList from "./VoucherList";
import VoucherDetail from "./VoucherDetail";
import IllumProvider from "../../providers/illumkey";
import Empty from "../../components/Empty";
import ScrollToTop from "../../components/ScrollToTop";
import Spinner from "../../components/Spinner";

const routeOptions = {
	routeName: "MINE VOUCHERS",
	hideHeader: false,
	showBackButton: false,
	scrollAnimation: true
};

class Keys extends PureComponent {
	constructor(props) {
		super(props);
		const loaded = props.vouchers.length > 0;
		this.state = {
			error: null,
			showVoucher: false,
			loaded: loaded,
			imagesLoaded: loaded
		};
		this.changeRoute(routeOptions);
		!loaded && this.fetchVouchers();
	}

	componentWillUnmount() {
		this.unmounted = true;
	}

	changeRoute = options => {
		const { changeRoute } = this.props;
		if (!this.unmounted) {
			changeRoute({ ...options });
		}
	};

	componentWillReceiveProps(nextprops) {
		const { match } = nextprops;
		const { id } = match.params;
		const { showVoucher } = this.state;
		if (!!id && !showVoucher) {
			this.showVoucher(true);
		} else if (!id && showVoucher) {
			this.showVoucher(false);
		}
	}

	fetchVouchers = async () => {
		const { getVouchers, changeRoute } = this.props;

		try {
			const data = await getVouchers();

			if (!this.unmounted) {
				this.setState({
					loaded: true
				});
			}

			const { showVoucher } = this.state;
			changeRoute({
				...routeOptions,
				showBackButton: showVoucher,
				vouchers: data
			});
		} catch (error) {
			if (!this.unmounted) {
				this.setState({
					loaded: true
				});
			}
		}
	};

	// responsible for marking as cashed in overview
	onVoucherCashed = id => {
		let { vouchers } = this.props;

		vouchers = vouchers.map(v => {
			if (v.id === id) {
				return {
					...v,
					cashed: true
				};
			}
			return v;
		});

		this.changeRoute({
			vouchers: vouchers
		});
		this.setState(state => ({
			loaded: true
		}));
	};

	showVoucher = show => {
		const { vouchers } = this.props;
		this.setState({
			showVoucher: show
		});
		this.changeRoute({
			...routeOptions,
			showBackButton: show,
			scrollAnimation: !show,
			loading: vouchers.length === 0 // if not fetched yet load
		});
	};

	onImagesLoaded = () => {
		const { showVoucher } = this.state;
		this.setState({
			imagesLoaded: true
		});
		this.changeRoute({
			...routeOptions,
			showBackButton: showVoucher,
			loading: false
		});
	};

	render() {
		const { cashVoucher, vouchers } = this.props;
		const { showVoucher, loaded, imagesLoaded } = this.state;

		if (vouchers.length === 0 && loaded) {
			return (
				<div className="vouchers-list route">
					<Empty message="No vouchers yet" />
				</div>
			);
		}

		return (
			<div className="black-background">
				<ScrollToTop />

				<Spinner noBg absolute active={!loaded} />

				<VoucherList
					{...this.state}
					vouchers={vouchers}
					onImagesLoaded={this.onImagesLoaded}
				/>
				<VoucherDetail
					{...this.props}
					active={showVoucher}
					vouchers={vouchers}
					cashVoucher={cashVoucher}
					changeRoute={this.changeRoute}
					onVoucherCashed={this.onVoucherCashed}
				/>
			</div>
		);
	}
}

export default routeChanger(IllumProvider(Keys));
