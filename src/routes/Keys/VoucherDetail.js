import React, { PureComponent } from "react";
import Swiper from "../../components/Swiper";
import IllumError from "../../utils/IllumError";
import NoScroll from "../../components/NoScroll";

class VoucherDetail extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			cashed: props.cashed,
			voucher: null
		};
	}

	cashVoucher = async cb => {
		const { cashVoucher, match, onVoucherCashed } = this.props;
		const { params } = match;
		const { voucher } = this.state;
		try {
			await cashVoucher({
				data: {
					voucher_id: voucher.id,
					type: voucher.type
				}
			});
			cb("INDLØST");
			onVoucherCashed(params.id);
		} catch (error) {
			const message =
				error instanceof IllumError ? error.message : "Noget gik galt";
			cb(message);
		}
	};

	componentWillReceiveProps(nextprops) {
		const { vouchers, match } = nextprops;

		const { params } = match;
		const voucher = vouchers.find(v => v.id === params.id);
		if (!!voucher) {
			this.setState({
				voucher
			});
		}
	}

	render() {
		const { active } = this.props;

		const { voucher } = this.state;
		if (!voucher) {
			return <div className="voucher-item">Voucher ikke fundet</div>;
		}

		const { cashed, thumb, name, enddate, description } = voucher;

		return (
			<div className={`voucher-item ${active ? "active" : ""}`}>
				{active ? <NoScroll /> : null}
				<div className="voucher-card">
					<figure className="voucher-img">
						<img src={thumb} alt="voucher" />
					</figure>
					<div className="voucher-content">
						<h3>{name}</h3>
						<p>{description}</p>
						<div className="expiry">
							{!!enddate ? `Udløber d. ${enddate}` : "Ingen udløbsdato"}
						</div>
					</div>
				</div>
				<p className="disclaimer">
					Du indløser din voucher ved kassen i ILLUM ved at lade en medarbejder
					swipe. Undlad at swipe selv, da din voucher indløses og dermed ikke
					længere kan benyttes i ILLUM.
				</p>
				<Swiper
					key={voucher.id}
					swipeText={"SWIPES AF MEDARBEJDER"}
					complete={cashed}
					completeText={"INDLØST"}
					loadingText={"INDLØSER..."}
					onComplete={this.cashVoucher}
				/>
			</div>
		);
	}
}

export default VoucherDetail;
