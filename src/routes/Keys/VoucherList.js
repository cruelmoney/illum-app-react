import React, { PureComponent } from "react";
import Voucher from "./Voucher";
import unique from "sugar/array/unique";
import intersect from "sugar/array/intersect";

class VoucherList extends PureComponent {
	state = {
		loaded: []
	};

	isLoaded = vouchers => {
		const { loaded } = this.state;
		const toLoad = vouchers.slice(0, 10).map(v => v.id);
		return intersect(toLoad, loaded).length >= toLoad.length;
	};

	onLoad = id => {
		const { onImagesLoaded, vouchers } = this.props;
		this.setState(({ loaded }) => {
			if (loaded.length > Math.min(vouchers.length, 10)) {
				!!onImagesLoaded && onImagesLoaded();
			}
			return {
				loaded: [...loaded, id]
			};
		});
	};

	render() {
		let { vouchers } = this.props;
		vouchers = unique(vouchers, v => v.id).reverse();
		const isLoaded = this.isLoaded(vouchers);

		return (
			<div className={`vouchers-list route loaded`} id="keys">
				<ul className="left">
					{!vouchers
						? null
						: vouchers.map(
								(v, idx) =>
									idx % 2 === 0 ? (
										<Voucher
											onLoad={this.onLoad}
											key={"voucher-" + v.id}
											{...v}
										/>
									) : null
						  )}
				</ul>
				<ul className="right">
					{!vouchers
						? null
						: vouchers.map(
								(v, idx) =>
									idx % 2 !== 0 ? (
										<Voucher
											onLoad={this.onLoad}
											key={"voucher-" + v.id}
											{...v}
										/>
									) : null
						  )}
				</ul>
			</div>
		);
	}
}

export default VoucherList;
