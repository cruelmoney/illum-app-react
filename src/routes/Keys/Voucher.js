import React, { PureComponent } from "react";
import { NavLink } from "react-router-dom";

export default class Voucher extends PureComponent {
	onLoad = e => {
		const { id, onLoad } = this.props;
		onLoad && onLoad(id);
	};

	render() {
		const { cashed, thumb, name, enddate, id } = this.props;

		return (
			<li className={`voucher ${cashed ? "cashed" : ""}`}>
				<NavLink to={"/keys/" + id}>
					<figure className="voucher-list-img">
						<img
							ref={i => {
								if (i && i.complete) this.onLoad(i);
							}}
							onLoad={this.onLoad}
							src={thumb}
							alt=""
						/>
						<figcaption>
							<h4>{name}</h4>
							<div className="expiry">
								{!!enddate ? `Udløber d. ${enddate}` : "Ingen udløbsdato"}
							</div>
						</figcaption>
					</figure>
				</NavLink>
			</li>
		);
	}
}
