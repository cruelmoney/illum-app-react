import React, { Component } from "react";
import routeChanger from "../../../../providers/routeChanger";
import illumProvider from "../../../../providers/illumkey";

const routeOptions = {
	routeName: "KONKURRENCEBETINGELSER",
	showBackButton: true,
	scrollAnimation: true,
	hideHeader: false
};

class Terms extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: []
		};
		this.changeRoute(routeOptions);
		this.fetchData();
	}

	componentWillUnmount() {
		this.unmounted = true;
	}

	changeRoute = options => {
		const { changeRoute } = this.props;
		if (!this.unmounted) {
			changeRoute(options);
		}
	};

	fetchData = async _ => {
		const { getTermsAndConditions } = this.props;
		this.changeRoute({
			...routeOptions,
			loading: true
		});

		const data = await getTermsAndConditions();

		this.setState({
			data: data
		});
		this.changeRoute({
			...routeOptions,
			loading: false
		});
	};

	render() {
		const { data } = this.state;

		return (
			<div className="container external-content page-template-pdflayout">
				<div className="row">
					<div className="col-sm-10 col-sm-push-1">
						<div className="row">
							<div className="ribbon-bg ribbon-alt col-sm-10 col-sm-push-1 col-xs-12" />
						</div>
						<div className="full">
							<div className="row">
								<div className="col-sm-8 col-sm-push-2 intro">
									<div dangerouslySetInnerHTML={{ __html: data }} />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default routeChanger(illumProvider(Terms));
