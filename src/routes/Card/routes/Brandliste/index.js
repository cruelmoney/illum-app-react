import React, { Component } from 'react';
import routeChanger from '../../../../providers/routeChanger';
import illumProvider from '../../../../providers/illumkey';


const routeOptions = {
  routeName:'Brandliste',
  showBackButton: true,
  scrollAnimation: true,
  hideHeader: false,
}

class Brandliste extends Component {

  constructor(props){
    super(props);
    this.state={
      data: []
    }
    this.changeRoute(routeOptions);
    this.fetchData();
  }

  componentWillUnmount(){
    this.unmounted = true;
  }

  changeRoute = (options) => {
    const {changeRoute} = this.props;
    if(!this.unmounted){
      changeRoute(options);
    }
  }


  fetchData = async _ => {
    const {getBrandList} = this.props;
    this.changeRoute({
      ...routeOptions,
      loading: true
    });

    const data = await getBrandList();
    
    this.setState({
      data: data
    });
    this.changeRoute({
      ...routeOptions,
      loading: false
    });
  }


  render() {
    const { data } = this.state;

    return (
      <div className="container external-content page-template-pdflayout">
        <div className="row">
          <div className="col-sm-10 col-sm-push-1">
            <div className="row">
              <div className="ribbon-bg ribbon-alt col-sm-10 col-sm-push-1 col-xs-12">
              </div>
            </div>
            <div className="full">
              <div className="row">
                <div className="col-sm-8 col-sm-push-2 intro">
                  <h1>Brands i illum, hvor du kan optjene og bruge dine point</h1>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 pattern">


                {
                  !data ? null : data.map((d,idx) => {
                    if(d.type === 'html'){
                      return( 
                        <React.Fragment key={'floor-'+idx}>
                          <div className="xlspacer"></div>
                          <h2>{d.rawtext}</h2>
                        </React.Fragment>
                      )
                    }else if(d.type === 'brandliste'){
                     return(
                        <React.Fragment key={'category-'+idx}>
                          <div className="spacer"></div>
                          <h3 className="illum-store">{d.headline}</h3>
                          <p className="columns">
                            {
                              d.brands.map((b,idx) => (
                                <React.Fragment key={'store-'+idx}>{b}<br/></React.Fragment>
                              ))
                            }
                          </p>
                        </React.Fragment>
                     )
                    }
                    return null;
                  })
                }
                <div className="xlspacer"></div>
              </div>
            </div>


          </div>
        </div>
      </div>
    )
  }
};

export default routeChanger(
  illumProvider(Brandliste)
);