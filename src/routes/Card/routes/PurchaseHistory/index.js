import React, { Component } from 'react';
import routeChanger from '../../../../providers/routeChanger';
import IllumProvider from '../../../../providers/illumkey';

const routeOptions = {
  routeName:'Købshistorik',
  showBackButton: true,
  scrollAnimation: true,
  hideHeader: false,
}

class Card extends Component {
  constructor(props){
    super(props);
    this.state = {
      transactions: [],
      show: 10,
      loaded: false
    }
    this.changeRoute(routeOptions);
    this.fetchData();
  }

  componentWillUnmount(){
    this.unmounted = true;
  }

  changeRoute = (options) => {
    const {changeRoute} = this.props;
    if(!this.unmounted){
      changeRoute(options);
    }
  }


  fetchData = async () => {
    const {getHistory} = this.props;
    this.changeRoute({
      ...routeOptions,
      loading: true
    });
    try {
      const data = await getHistory();
      this.setState({
        transactions: data.Transaction,
        loaded: true
      });
      this.changeRoute({
        ...routeOptions,
        loading: false
      });

    } catch (error) {
      this.setState({
        loaded: true,
        loading: false
      });
    }
    
  }

  showAllTransactions = () => {
    this.setState(state=>({
      show: state.transactions.length
    }))
  }

  render() {
    let {transactions, show} = this.state;
    transactions = transactions.slice(0, show);

    return (
      <div
          className="purchase-history show-mobile"
         > 
        <h2>
          Købsoversigt
        </h2>
        {transactions.length === 0 ? <p>Ingen købshistorik tilgængelig i øjeblikket</p> : 
          transactions.map((t, idx) => (
            <div key={"transaction-"+idx} className="info-box-small">
              <div className="row">
                {t.TransactionType === 2 ? "Promotion points" : t.TillText}
              </div>
              <div className="row">
              <div className="time">
                {t.TransactionTime}
              </div>
                {t.PurchaseAmount}
              </div>
              <div className="row">
                {t.Points} point
              </div>
              <div className="row">
              (Værdi af point:{t.Amount})
              </div>
            </div>
          ))
        }
        {transactions.length === 0 ? null :
          <button onClick={this.showAllTransactions} className="show-table-btn">Se alle køb</button>
        }
      
      </div>
    )
  }
};

export default routeChanger(
  IllumProvider(Card)
);