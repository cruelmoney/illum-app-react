import React, { Component } from "react";
import routeChanger from "../../../../providers/routeChanger";
import illumProvider from "../../../../providers/illumkey";

class Dynamic extends Component {
	constructor(props) {
		super(props);

		this.routeOptions = {
			routeName: props.name,
			showBackButton: true,
			scrollAnimation: true,
			hideHeader: false
		};

		this.changeRoute(this.routeOptions);
	}

	componentWillUnmount() {
		this.unmounted = true;
	}

	changeRoute = options => {
		const { changeRoute } = this.props;
		if (!this.unmounted) {
			changeRoute(options);
		}
	};

	render() {
		const { brandlists, content, hero, footer } = this.props;

		return (
			<div className="container external-content page-template-pdflayout">
				<div className="row">
					<div className="col-sm-10 col-sm-push-1">
						<div className="row">
							<div className="ribbon-bg ribbon-alt col-sm-10 col-sm-push-1 col-xs-12" />
						</div>
						{hero && (
							<div className="full">
								<div className="row">
									<div className="col-sm-8 col-sm-push-2 intro">
										{hero.map((c, idx) => (
											<div
												key={idx}
												dangerouslySetInnerHTML={{ __html: c.content }}
											/>
										))}
									</div>
								</div>
							</div>
						)}
						{brandlists && getBrandListMarkup(brandlists)}
						<div className="row">
							<div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 pattern">
								{content &&
									content.map((c, idx) => (
										<div
											key={idx}
											dangerouslySetInnerHTML={{ __html: c.content }}
										/>
									))}
								<div className="xlspacer" />
							</div>
						</div>
						{footer && (
							<div className="full">
								<div className="row">
									<div className="col-sm-8 col-sm-push-2 intro">
										{footer.map((c, idx) => (
											<div
												key={idx}
												dangerouslySetInnerHTML={{ __html: c.content }}
											/>
										))}
									</div>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default routeChanger(illumProvider(Dynamic));

const getBrandListMarkup = data => {
	{
		return data.map((d, idx) => {
			if (d.type === "html") {
				return (
					<React.Fragment key={"floor-" + idx}>
						<div className="xlspacer" />
						<div dangerouslySetInnerHTML={{ __html: d.content }} />
					</React.Fragment>
				);
			} else if (d.type === "brandliste") {
				return (
					<React.Fragment key={"category-" + idx}>
						<div className="spacer" />
						<h3 className="illum-store">{d.headline}</h3>
						<p className="columns">
							{d.brands.map((b, idx) => (
								<React.Fragment key={"store-" + idx}>
									{b}
									<br />
								</React.Fragment>
							))}
						</p>
					</React.Fragment>
				);
			}
			return null;
		});
	}
};
