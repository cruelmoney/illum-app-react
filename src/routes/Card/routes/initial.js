import React, { Component } from "react";
import routeChanger from "../../../providers/routeChanger";
import { NavLink } from "react-router-dom";
import ExternalLink from "../../../components/ExternalLink";
import JsBarcode from "jsbarcode";
import logo from "../../../assets/img/ik-logo.png";

const routeOptions = {
	routeName: "Key card",
	hideHeader: false,
	showBackButton: false
};

class Card extends Component {
	constructor(props) {
		super(props);
		const { changeRoute } = this.props;
		changeRoute(routeOptions);
		this.barcode = React.createRef();
	}

	componentDidMount() {
		JsBarcode(this.barcode.current, "barcode", {
			background: "#1C1C1B",
			lineColor: "#fff",
			height: 25,
			width: 1,
			displayValue: false,
			margin: 0
		});
	}

	render() {
		const { match, profile, navItems } = this.props;
		const { name, memberno } = profile;

		return (
			<div className="key-card" id="card">
				<div className="card-wrapper">
					<div className="card">
						<div className="card-details">
							<div className="barcode">
								<div className="logo">
									<img src={logo} alt="logo" />
								</div>
								<svg ref={this.barcode} id="barcode" />
							</div>
							<div className="member">
								<span>{name}</span> <span>{memberno}</span>
							</div>
							<div className="card-address">
								<span>ILLUM</span> <span>Østergade 52</span>{" "}
								<span>DK-1001</span> <span>København K</span>{" "}
								<span>+45 33 14 40 02</span> <span>illum.dk</span>
							</div>
						</div>
					</div>
				</div>
				<ul className="links">
					<li>
						<NavLink to={`${match.url}/purchase-history`}>Mine køb</NavLink>
					</li>
					<li>
						<ExternalLink
							data-external="yes"
							href="https://illum.onlinebooq.dk"
						>
							Book services
						</ExternalLink>
					</li>
					{/* <li>
						<NavLink to={`${match.url}/brandliste`}>
							Hvor kan mine point bruges?
						</NavLink>
					</li> */}
					{/* <li>
						<NavLink to={`${match.url}/terms`}>Konkurrencebetingelser</NavLink>
					</li> */}
					{navItems &&
						navItems.map((n, idx) => (
							<li key={idx}>
								<NavLink to={n.to}>{n.label}</NavLink>
							</li>
						))}
				</ul>
			</div>
		);
	}
}

export default routeChanger(Card);
