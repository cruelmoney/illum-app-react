import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Card from "./routes/initial";
import PurchaseHistory from "./routes/PurchaseHistory";
import Brandliste from "./routes/Brandliste";
import Terms from "./routes/Terms";
import illumProvider from "../../providers/illumkey";
import { Object } from "core-js";
import Dynamic from "./routes/Dynamic";

const dataToRoutes = (data, baseUrl) => {
	return Object.values(data).map((r, idx) => (
		<Route
			key={idx}
			path={`${baseUrl}/dynamic-${idx}`}
			render={({ match }) => <Dynamic match={match} {...r} />}
		/>
	));
};

const dataToNavItems = (data, baseUrl) => {
	return Object.values(data).map((r, idx) => ({
		to: `${baseUrl}/dynamic-${idx}`,
		label: r.name
	}));
};

class Index extends Component {
	state = { data: [] };

	componentDidMount = async () => {
		const { getContent } = this.props;
		const data = await getContent();
		console.log(data);
		this.setState({
			data
		});
	};

	render() {
		const { match } = this.props;

		return (
			<Switch>
				<Route
					exact
					path={`${match.url}`}
					render={({ match }) => (
						<Card
							match={match}
							navItems={dataToNavItems(this.state.data, match.url)}
						/>
					)}
				/>
				<Route
					path={`${match.url}/purchase-history`}
					component={PurchaseHistory}
				/>
				{/* <Route path={`${match.url}/brandliste`} component={Brandliste} />
				<Route path={`${match.url}/terms`} component={Terms} /> */}
				{dataToRoutes(this.state.data, match.url)}
			</Switch>
		);
	}
}

export default illumProvider(Index);
