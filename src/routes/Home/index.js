import React, { PureComponent } from 'react';
import routeChanger from '../../providers/routeChanger';
import IllumProvider from '../../providers/illumkey';
import Spinner from '../../components/Spinner';
import Empty from '../../components/Empty';
import VoucherList from '../Keys/VoucherList';
import Challenge from '../../components/Challenge';
import ScrollToTop from '../../components/ScrollToTop';
import moment from 'moment';

const routeOptions = {
  routeName:'Home',
  hideHeader: true
}

class Home extends PureComponent {
  constructor(props){
    super(props);
    const {changeRoute} = props;
    const loaded = props.vouchers.length > 0;
    changeRoute(routeOptions);
    this.state={
      loaded,
      imagesLoaded : loaded
    }
    !loaded && this.fetchVouchers();
  }

  fetchVouchers = async () => {
    const {getVouchers, changeRoute} = this.props;

     try {
      const data = await getVouchers();
      changeRoute({
        vouchers: data,
      });
      this.setState({
        loaded: true
      });
     } catch (error) {
        this.setState({
          loaded: true
        });
     }
  }

  onChallengeComplete = async () => {
    const {changeRoute, checkLogin, onLogin} = this.props;
    // get newly last played
    const profile = await checkLogin({returnProfile: true});
    onLogin(profile);
    await this.fetchVouchers();
    changeRoute({
      ...routeOptions,
      profile
    });
    // update user to get last played
  }

  canPlay = () => {
    if(!window.in_app) return true;
    
    const {profile} = this.props;
    if(profile.lastplayed){
      const last = moment.unix(profile.lastplayed);
      const now = moment();
      if(last.isSame(now, 'day')){
        return false;
      }
    }
    return true;
  }

  onImagesLoaded = () => {
    this.setState({
      imagesLoaded : true
    });
  }

  render() {
    const {loaded, imagesLoaded} = this.state;
    const {vouchers} = this.props;

    return (
      <div className="intro" id="home">
          <ScrollToTop />
          <Challenge 
            canPlay={this.canPlay()}
            onComplete={this.onChallengeComplete} 
          />

          <div className="home-vouchers">
              
          

           {
             (vouchers.length === 0 && loaded && imagesLoaded) ? (
                  <Empty message="No vouchers yet" />
              ) : null 
            }
            
           {
             (loaded) ? (
              <VoucherList 
                {...this.state}
                vouchers={vouchers}
                onImagesLoaded={this.onImagesLoaded}
              />
              ) : (
              <Spinner 
                noBg 
                absolute 
                active={!loaded || !imagesLoaded}
              /> 
            )}
          </div>
      </div>
    )
  }
};

export default routeChanger(
  IllumProvider(Home)
);