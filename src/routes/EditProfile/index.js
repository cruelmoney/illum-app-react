import React, { Component } from "react";
import routeChanger from "../../providers/routeChanger";
import illumProvider from "../../providers/illumkey";
import DatePicker from "../../components/DatePicker";
import moment from "moment";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";
import Select from "react-validation/build/select";
import * as validator from "../../utils/validators";
import IllumError from "../../utils/IllumError";
import ExternalLink from "../../components/ExternalLink";

const routeOptions = {
	routeName: "Rediger profil",
	showBackButton: true,
	scrollAnimation: true,
	hideHeader: false
};
class EditProfile extends Component {
	constructor(props) {
		super(props);
		const { changeRoute } = props;
		changeRoute(routeOptions);
		let birthday = null;
		if (!!props.profile.birthday && props.profile.birthday !== "Ukendt") {
			birthday = moment(props.profile.birthday, "DD/MM/YYYY");
		}

		console.log({ profile: props.profile });

		this.state = {
			...props.profile,
			birthday,
			error: ""
		};
		this.form = React.createRef();
		this.formButton = React.createRef();
	}

	logOut = () => {
		const { changeRoute } = this.props;
		changeRoute({ profile: {}, vouchers: [] });
	};

	handleChange = e => {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		});
	};

	submit = async e => {
		e.preventDefault();
		const self = this;
		const { updateUser, changeRoute } = this.props;
		changeRoute({
			...routeOptions,
			loading: true
		});
		this.form.current.validateAll();
		const values = this.form.current.getValues();
		let { birthday } = this.state;
		if (!!birthday && birthday !== "Ukendt") {
			birthday = birthday.format("YYYY-MM-DD");
		}

		const data = {
			...values,
			birthday,
			newpassword: values.confirmPassword
		};

		try {
			const profile = await updateUser({ data });
			let { birthday } = profile;
			if (!!birthday && birthday !== "Ukendt") {
				birthday = moment(birthday, "DD/MM/YYYY");
			}
			self.setState({
				...profile,
				birthday,
				error: ""
			});
			changeRoute({
				...routeOptions,
				loading: false,
				profile
			});
			self.showError("Din profil er opdateret");
		} catch (error) {
			changeRoute({ ...routeOptions, loading: false });
			const message =
				error instanceof IllumError ? error.message : "Noget gik galt";
			console.log(message);
			self.showError(message);
		}
	};

	showError = message => {
		this.setState({
			error: message
		});
	};

	removeError = () => {
		this.setState({
			error: ""
		});
	};

	render() {
		const { name, email, gender, phone, birthday, error } = this.state;

		return (
			<div className="edit-profile">
				<Form ref={this.form} onSubmit={this.submit}>
					<fieldset>
						<div className="form-group">
							<label htmlFor="name" className="control-label">
								Navn
							</label>
							<Input
								type="text"
								id="name"
								name="name"
								value={name}
								className="form-control ccm-input-text"
								validations={[validator.required]}
							/>
						</div>

						<div className="form-group">
							<label htmlFor="email" className="control-label">
								E-mail
							</label>
							<Input
								type="text"
								id="email"
								name="email"
								value={email}
								className="form-control ccm-input-text"
								validations={[validator.required, validator.email]}
							/>
						</div>

						<div className="ccm-profile-attribute">
							<div className="form-group">
								<label htmlFor="phone" className="control-label">
									Telefon
								</label>
								<Input
									type="tel"
									id="phone"
									name="phone"
									value={phone}
									autoComplete="off"
									validations={[validator.required]}
								/>
							</div>
						</div>

						<div className="ccm-profile-attribute">
							<div className="form-group">
								<label htmlFor="birthday" className="control-label">
									Fødselsdag
								</label>
								<DatePicker
									name="birthday"
									selected={birthday}
									onChange={m =>
										this.handleChange({
											target: {
												name: "birthday",
												value: m
											}
										})
									}
								/>
							</div>
						</div>

						<div className="ccm-profile-attribute">
							<div className="form-group">
								<label htmlFor="gender" className="control-label">
									Køn
								</label>
								<Select
									value={gender}
									id="gender"
									name="gender"
									ccm-passed-value="2"
									className="form-control"
									onChange={this.handleChange}
								>
									<option value="">KØN</option>
									<option value="Kvinde">Kvinde</option>
									<option value="Mand">Mand</option>
								</Select>
							</div>
						</div>
						<div className="form-group">
							<Button type="submit" name="save" className="appbtn">
								Gem
							</Button>
							<span className="error">{error}</span>
						</div>
					</fieldset>

					<br />

					<fieldset>
						<h2>Ændre kodeord</h2>
						<div className="form-group">
							<label htmlFor="password" className="control-label">
								Nyt kodeord
							</label>
							<Input
								type="password"
								id="password"
								name="password"
								value=""
								autoComplete="off"
								className="form-control ccm-input-password"
							/>
						</div>

						<div className="form-group">
							<label htmlFor="confirmPassword" className="control-label">
								Bekræft nyt kodeord
							</label>
							<Input
								type="password"
								id="confirmPassword"
								name="confirmPassword"
								value=""
								autoComplete="off"
								className="form-control ccm-input-password"
								validations={[validator.password]}
							/>
						</div>
					</fieldset>

					<div className="form-group">
						<Button
							ref={this.formButton}
							type="submit"
							name="save"
							value=""
							lassName="appbtn"
						>
							Gem
						</Button>
						<span className="error">{error}</span>
					</div>
				</Form>

				<div className="form-group">
					<ExternalLink
						className="external"
						href="https://key.illum.dk/user-dashboard"
					>
						Se og rediger din fulde ILLUM KEY profil på Mit ILLUM KEY.
					</ExternalLink>
				</div>
			</div>
		);
	}
}

export default routeChanger(illumProvider(EditProfile));
